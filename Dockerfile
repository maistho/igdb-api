FROM node:20-alpine as deps

WORKDIR /app

COPY package.json package-lock.json ./
RUN npm ci

FROM deps as builder

COPY tsconfig.json ./
COPY src ./src
RUN npm run build

FROM deps as prod

RUN npm ci --omit dev

FROM node:alpine

WORKDIR /app

COPY --from=prod /app/node_modules ./node_modules
COPY --from=builder /app/build ./build

RUN mkdir -p /app/image-cache && chown -R node:node /app/image-cache
USER node

ENV NODE_ENV=production
ENV LOG_LEVEL=info

ENV PORT=3000
EXPOSE 3000

ENTRYPOINT ["node", "build/main.js"]
