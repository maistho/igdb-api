// I moved prismjs types into here and removed the parts which I don't use since some parts otherwise would require 'dom' lib typings

// Type definitions for prismjs 1.16
// Project: http://prismjs.com/, https://github.com/leaverou/prism
// Definitions by: Michael Schmidt <https://github.com/RunDevelopment>
//                 ExE Boss <https://github.com/ExE-Boss>
//                 Erik Lieben <https://github.com/eriklieben>
//                 Andre Wiggins <https://github.com/andrewiggins>
//                 Michał Miszczyszyn <https://github.com/mmiszy>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.8

declare module 'prismjs' {
  export const languages: Languages

  /**
   * Low-level function, only use if you know what you’re doing. It accepts a string of text as input
   * and the language definitions to use, and returns a string with the HTML produced.
   *
   * The following hooks will be run:
   * 1. `before-tokenize`
   * 2. `after-tokenize`
   * 3. `wrap`: On each {@link Prism.Token}.
   *
   * @param text A string with the code to be highlighted.
   * @param grammar An object containing the tokens to use.
   *
   * Usually a language definition like `Prism.languages.markup`.
   * @param language The name of the language definition passed to `grammar`.
   * @returns The highlighted HTML.
   *
   * @example
   * Prism.highlight('var foo = true;', Prism.languages.js, 'js');
   */
  export function highlight(
    text: string,
    grammar: Grammar,
    language: string,
  ): string

  export type GrammarValue = RegExp | TokenObject | Array<RegExp | TokenObject>
  export type Grammar = GrammarRest & Record<string, GrammarValue>
  export interface GrammarRest {
    keyword?: GrammarValue
    number?: GrammarValue
    function?: GrammarValue
    string?: GrammarValue
    boolean?: GrammarValue
    operator?: GrammarValue
    punctuation?: GrammarValue
    atrule?: GrammarValue
    url?: GrammarValue
    selector?: GrammarValue
    property?: GrammarValue
    important?: GrammarValue
    style?: GrammarValue
    comment?: GrammarValue
    'class-name'?: GrammarValue

    /**
     * An optional grammar object that will appended to this grammar.
     */
    rest?: Grammar
  }

  /**
   * The expansion of a simple `RegExp` literal to support additional properties.
   */
  export interface TokenObject {
    /**
     * The regular expression of the token.
     */
    pattern: RegExp

    /**
     * If `true`, then the first capturing group of `pattern` will (effectively) behave as a lookbehind
     * group meaning that the captured text will not be part of the matched text of the new token.
     */
    lookbehind?: boolean

    /**
     * Whether the token is greedy.
     *
     * @default false
     */
    greedy?: boolean

    /**
     * An optional alias or list of aliases.
     */
    alias?: string | string[]

    /**
     * The nested tokens of this token.
     *
     * This can be used for recursive language definitions.
     *
     * Note that this can cause infinite recursion.
     */
    inside?: Grammar
  }
  export type Languages = LanguageMapProtocol & LanguageMap
  export interface LanguageMap {
    /**
     * Get a defined language's definition.
     */
    [language: string]: Grammar
  }
  export interface LanguageMapProtocol {
    /**
     * Creates a deep copy of the language with the given id and appends the given tokens.
     *
     * If a token in `redef` also appears in the copied language, then the existing token in the copied language
     * will be overwritten at its original position.
     *
     * @param id The id of the language to extend. This has to be a key in `Prism.languages`.
     * @param redef The new tokens to append.
     * @returns The new language created.
     * @example
     * Prism.languages['css-with-colors'] = Prism.languages.extend('css', {
     *     'color': /\b(?:red|green|blue)\b/
     * });
     */
    extend(id: string, redef: Grammar): Grammar

    /**
     * Inserts tokens _before_ another token in a language definition or any other grammar.
     *
     * As this needs to recreate the object (we cannot actually insert before keys in object literals),
     * we cannot just provide an object, we need an object and a key.
     *
     * If the grammar of `inside` and `insert` have tokens with the same name, the tokens in `inside` will be ignored.
     *
     * All references of the old object accessible from `Prism.languages` or `insert` will be replace with the new one.
     *
     * @param inside The property of `root` that contains the object to be modified.
     *
     * This is usually a language id.
     * @param before The key to insert before.
     * @param insert An object containing the key-value pairs to be inserted.
     * @param [root] The object containing `inside`, i.e. the object that contains the object that will be modified.
     *
     * Defaults to `Prism.languages`.
     * @returns The new grammar created.
     * @example
     * Prism.languages.insertBefore('markup', 'cdata', {
     *     'style': { ... }
     * });
     */
    insertBefore(
      inside: string,
      before: string,
      insert: Grammar,
      root: LanguageMap,
    ): Grammar
  }
}
