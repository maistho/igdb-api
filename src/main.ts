import { start, stop } from './server'
import {
  setupExitSignalHandlers,
  setupUnhandledErrorHandlers,
} from './util/exit-handlers'

setupExitSignalHandlers(async () => {
  // Add any teardown here
  await stop()
})
setupUnhandledErrorHandlers(async () => {
  // Add any teardown here
  await stop()
})

start()
