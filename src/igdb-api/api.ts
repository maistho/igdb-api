import { NotFound } from 'http-errors'

import path from 'path'
import { existsSync } from 'fs'
import { writeFile, mkdir } from 'fs/promises'
declare const fetch: typeof import('undici').fetch

import Bottleneck from 'bottleneck'

const limiter = new Bottleneck({
  minTime: 300, // Minimum time between requests in milliseconds (1000ms / 4 requests = 250ms. Add some extra)
})

export type ImageSize =
  | 't_cover_big_2x'
  | 't_cover_big'
  | 't_cover_small_2x'
  | 't_cover_small'

class IGDBApi {
  private imageCachePath = path.resolve('image-cache')

  constructor(
    private readonly clientId: string,
    private readonly clientSecret: string,
  ) {
    if (!existsSync(this.imageCachePath)) {
      mkdir(this.imageCachePath, { recursive: true })
    }
  }

  private auth = { access_token: '', expires_at: -1 }

  private async authorizationHeader() {
    if (this.auth.expires_at > Date.now()) {
      return `Bearer ${this.auth.access_token}`
    }
    const res: any = await fetch(
      `https://id.twitch.tv/oauth2/token?client_id=${this.clientId}&client_secret=${this.clientSecret}&grant_type=client_credentials`,
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
        },
      },
    ).then((x) => x.json())
    this.auth = {
      access_token: res.access_token,
      expires_at: Date.now() + res.expires_in - 60_000,
    }
    return `Bearer ${this.auth.access_token}`
  }

  public async search(
    query: string,
  ): Promise<
    { id: string | number; name: string; imageId: string | undefined | null }[]
  > {
    const res: any = await fetch(
      `https://www.igdb.com/search_autocomplete_all?q=${encodeURIComponent(
        query,
      )}`,
    ).then((response) => response.json() as any)
    console.log(res)

    return (
      res?.game_suggest?.map((g: any) => ({
        id: g.id,
        name: g.name,
        imageId: g.cloudinary,
      })) ?? []
    )
  }

  public async searchWithProperApi(query: string) {
    return limiter.schedule(async () => {
      const res: any[] = await fetch('https://api.igdb.com/v4/games', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Client-ID': this.clientId,
          Authorization: await this.authorizationHeader(),
        },
        body: `search "${query}"; fields id,name,cover.image_id;`,
      }).then((response) => response.json() as any)
      console.log(res)

      return res.map((g) => ({
        id: g.id,
        name: g.name,
        imageId: g?.cover?.image_id,
      }))
    })
  }

  public async saveImage(imageId: string, size: ImageSize) {
    const imagePath = path.join(this.imageCachePath, `${size}-${imageId}.png`)
    if (!existsSync(imagePath)) {
      const res = await fetch(
        `https://images.igdb.com/igdb/image/upload/${size}/${imageId}.png`,
        {
          headers: {
            Accept: 'image/png',
            'Client-ID': this.clientId,
            Authorization: await this.authorizationHeader(),
          },
        },
      )
      if (res.status !== 200) {
        throw new NotFound(
          `Image https://images.igdb.com/igdb/image/upload/${size}/${imageId}.png doesn't exist`,
        )
      }
      await writeFile(imagePath, res.body!)
    }

    return imagePath
  }
}

export const api = new IGDBApi(
  process.env.IGDB_CLIENT_ID!,
  process.env.IGDB_CLIENT_SECRET!,
)
