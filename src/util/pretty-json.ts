import { FastifyPluginCallback } from 'fastify'
import fastifyPlugin from 'fastify-plugin'
import { ReadStream, readFileSync } from 'fs'
import { highlight, languages as prismLanguages } from 'prismjs'
import 'prismjs/components/prism-json'

const themes = {
  coy: 'prism-coy.css',
  dark: 'prism-dark.css',
  funky: 'prism-funky.css',
  okaidia: 'prism-okaidia.css',
  solarized: 'prism-solarizedlight.css',
  tomorrow: 'prism-tomorrow.css',
  twilight: 'prism-twilight.css',
  default: 'prism.css',
} as const

interface PrettyOptions {
  theme?: keyof typeof themes
}

/** Beautify requests which are sent directly from a browser (e.g. they prefer text/html over application/json) */
const prettyJsonFn: FastifyPluginCallback<PrettyOptions> = (
  app,
  options,
  done,
) => {
  const prismCss = readFileSync(
    `node_modules/prismjs/themes/${themes[options.theme ?? 'okaidia']}`,
  ).toString('utf-8')

  app.addHook('onSend', async (req, reply, payload) => {
    if (
      payload == null ||
      payload === '' ||
      typeof (payload as any)?.filename === 'string' ||
      payload instanceof ReadStream
    ) {
      return
    }
    const accept = req.accepts()
    switch (accept.type(['json', 'html'])) {
      case 'json':
        return payload
      case 'html': {
        reply.type('text/html')
        return `<html>
<head>
<style>${prismCss}</style>
<body style="font-size: 87.5%; margin: 0; background: #272822;">
<pre style="box-sizing: border-box; min-height: 100vh; margin: 0; border-radius: 0;" class="language-json"><code class="language-json">${highlight(
          JSON.stringify(JSON.parse(payload as string), undefined, 2),
          prismLanguages.json,
          'json',
        )}</code></pre>
</body>
</html>`
      }
    }
  })
  done()
}

export const prettyJson = fastifyPlugin(prettyJsonFn, {
  dependencies: ['@fastify/accepts'],
})
