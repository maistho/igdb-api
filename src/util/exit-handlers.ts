import { logger } from './logger'

export function setupExitSignalHandlers(
  handler: () => Promise<void> | void,
): void {
  // The signals we want to handle
  // NOTE: although it is tempting, the SIGKILL signal (9) cannot be intercepted and handled
  const SIGNALS = {
    SIGHUP: 1,
    SIGINT: 2,
    SIGTERM: 15,
  } as const

  // Create a listener for each of the signals that we want to handle
  Object.entries(SIGNALS).forEach(([signal, value]) => {
    process.on(signal, async () => {
      logger.info(`process received a ${signal} signal`)

      // Do any necessary shutdown logic in the provided handler
      await handler()

      logger.info(`server stopped by ${signal} with value ${value}`)
      process.exit(128 + value)
    })
  })
}

export function setupUnhandledErrorHandlers(
  handler: () => Promise<void> | void,
): void {
  const signals = ['uncaughtException', 'unhandledRejection'] as const

  signals.forEach((s) => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    process.on(s, async (error: any) => {
      logger.error(error)
      await handler()
      process.exit(-1)
    })
  })
}
