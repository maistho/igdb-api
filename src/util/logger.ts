import pino, { Bindings, Logger } from 'pino'

export const logger = pino({
  level: process.env.LOG_LEVEL ?? 'debug',
})

export function createLogger(
  bindings: Bindings,
  baseLogger: Logger = logger,
): Logger {
  return baseLogger.child(bindings)
}
