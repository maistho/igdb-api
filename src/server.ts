import fastify from 'fastify'
import fastifyAccepts from '@fastify/accepts'
import fastifyCors from '@fastify/cors'
import { prettyJson } from './util/pretty-json'
import { routes } from './routes'

const app = fastify({ logger: true })

app.register(fastifyAccepts)
app.register(prettyJson)
app.register(fastifyCors)

app.get('/', async () => {
  return { hello: 'world', timestamp: new Date().toISOString() }
})

app.register(routes)

export async function start(): Promise<void> {
  try {
    await app.listen({
      port: +(process.env.PORT ?? '3000'),
      host: '0.0.0.0',
    })
  } catch (err) {
    app.log.error(err)
    process.exit(1)
  }
}
export async function stop(): Promise<void> {
  await app.close()
}
