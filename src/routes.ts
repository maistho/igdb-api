import fs from 'fs'
import { FastifyPluginAsync } from 'fastify'
import { ImageSize, api } from './igdb-api/api'

export const routes: FastifyPluginAsync = async (app) => {
  // Add your routes here

  app.get<{ Querystring: { q: string } }>(
    '/search',
    {
      schema: {
        querystring: {
          type: 'object',
          additionalProperties: true,
          required: ['q'],
          properties: { q: { type: 'string', pattern: '^[^;"]+$' } },
        },
      },
    },
    async (req) => {
      const q = req.query.q
      const games = await api.searchWithProperApi(q)
      return games.map((g) => ({
        id: g.id,
        name: g.name,
        imageUrl: g.imageId
          ? `${req.protocol}://${req.headers.host}/images/t_cover_big_2x/${g.imageId}.png`
          : null,
        smallImageUrl: g.imageId
          ? `${req.protocol}://${req.headers.host}/images/t_cover_small_2x/${g.imageId}.png`
          : null,
      }))
    },
  )
  app.get<{ Params: { size: 't_cover_big_2x'; imageId: string } }>(
    '/images/:size/:imageId.png',
    {
      schema: {
        params: {
          type: 'object',
          additionalProperties: false,
          required: ['size', 'imageId'],
          properties: {
            size: {
              type: 'string',
              enum: [
                't_cover_big_2x',
                't_cover_big',
                't_cover_small_2x',
                't_cover_small',
              ] satisfies ImageSize[],
            },
            imageId: { type: 'string' },
          },
        },
      },
    },
    async (req, res) => {
      const { size, imageId } = req.params
      const imagePath = await api.saveImage(imageId, size)
      const stream = fs.createReadStream(imagePath)
      return res.code(200).type('image/png').send(stream)
    },
  )
}
